<?php
/**
 * Footer template (footer.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
?>


<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 pull-right col-xs-12 sitemap">
            	<?php echo wp_nav_menu(array('menu' => 'Footer-Menu', 'menu_class'  => 'footer-menu', 'theme_location' => 'primary' )); ?>
            </div>
            <div class="col-md-6 col-xs-12 copyright">Copyright © 2017 Makeithappen.com - All Rights Reserved</div>
        </div>
    </div>
</footer>


<div class="overlay"></div>

<div class="popup register-popup" id="register-popup">
    <div class="container">
        <div class="row">

            <div class="col-md-3 col-xs-12">
                <h3>ĐĂNG KÍ THÀNH VIÊN<br/> <strong>MAKE<span>IT</span>HAPPEN</strong></h3>
                <h4 id="mess-response" style="display:none;"></h4>
            </div>

            <div class="col-md-6 col-xs-12">
                <form action="">

                    <h3>Thông tin đăng nhập</h3>

                    <div class="form-inner">
                        <div class="form-inner-block half-inner">
                            <input type="text" id="inner_username" placeholder="Tên đăng nhập">
                            <div class="alert-custom">Vui lòng nhập tên đăng nhập và không có khoản cách</div>
                        </div>
                        <div class="form-inner-block half-inner">
                            <input type="text" id="inner_mail" placeholder="Email">
                            <div class="alert-custom">Vui lòng nhập email và không có khoản cách</div>
                        </div>

                    </div>


                    <div class="form-inner">
                        <div class="form-inner-block half-inner">
                            <input type="password" id="inner_password" placeholder="Mật khẩu">
                            <div class="alert-custom">Vui lòng nhập mật khẩu</div>
                        </div>
                        <div class="form-inner-block half-inner">
                            <input type="password" id="inner_repassword" placeholder="Đánh lại mật khẩu">
                            <div class="alert-custom">Vui lòng nhập lại đúng mật khẩu</div>
                        </div>

                    </div>

                    <hr>

                    <h3>Thông tin cá nhân</h3>
                    <div class="form-inner">
                        <div class="form-inner-block half-inner">
                            <input type="text" id="inner_firstname" placeholder="Tên">
                            <div class="alert-custom">Vui lòng nhập tên</div>
                        </div>
                        <div class="form-inner-block half-inner">
                            <input type="text" id="inner_lastname" placeholder="Họ">
                            <div class="alert-custom">Vui lòng nhập họ</div>
                        </div>
                    </div>

                    <div class="form-inner">
                        <div class="form-inner-block half-inner">
                            <input type="text" id="inner_phone" placeholder="Số điện thoại">
                            <div class="alert-custom">Vui lòng nhập số điện thoại</div>
                        </div>

                        <div class="form-inner-block quarter-inner">
                            <select name="gender" id="inner_gender">
                                <option value="0">Giới tính</option>
                                <option value="1">Nam</option>
                                <option value="2">Nữ</option>
                                <option value="3">Khác</option>
                            </select>
                            <div class="alert-custom">Vui lòng chọn giới tính</div>
                        </div>

                        <div class="form-inner-block quarter-inner">
                            <input type='text' id="inner_dob" placeholder="Chọn ngày sinh"/>
                            <div class="alert-custom">Vui lòng chọn ngày sinh</div>
                        </div>
                    </div>

                    <div class="form-inner">
                        <div class="form-inner-block full-inner">
                            <input type="text" id="inner_address" class="full-inner" placeholder="Địa chỉ">
                            <div class="alert-custom">Vui lòng nhập địa chỉ</div>
                        </div>
                    </div>

                </form>

                <div class="action-form">
                    <div class="agree">
                        <input type="checkbox" id="agree_rules">
                        <span>Tôi đồng ý với các điều kiện & điều khoản</span>
                        <div class="alert-custom">Vui lòng check</div>
                    </div>

                    <input id="btn-sign-up" type="button" class="sign-in-btn send-form half" value="SIGN UP">
                </div>


            </div>

            <div class="col-md-3 col-xs-12">
                <h3>ĐĂNG NHẬP BẰNG <strong>FACEBOOK</strong></h3>
                <a href="#" class="sign-in-btn"><i class="fa fa-facebook" aria-hidden="true"></i> CONNECT</a>
            </div>
        </div>
    </div>

    <div class="close-popup"><i class="fa fa-times" aria-hidden="true"></i></div>
</div>

<div class="back-to-top">LÊN ĐẦU TRANG <i class="fa fa-long-arrow-up" aria-hidden="true"></i></div>
<!-- /#Configurator -->

<script src="<?php bloginfo('template_url')?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/configurator-dependencies.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>

<script src="<?php bloginfo('template_url')?>/js/main.js"></script>

<script src="<?php bloginfo('template_url')?>/js/q-custom.js"></script>



<script>
    $(document).on('ready', function () {
        changeTab();


        // active slider
        $('.slider-fans-box').slick({
            autoplay: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 2,
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
        });

        $('.news-slider').slick({
            autoplay: true,
            dots: true,
            infinite: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
        });

        $('.banner-section-slider').slick({
            autoplay: true,
            infinite: true,
            speed: 1000,
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
        });

    });

    function changeTab() {
        $('.nav-filter li').on('click touch', function (event) {
            event.preventDefault();

            var $this = $(this), target_this = $this.attr('data-tab');

            $this.addClass('active').siblings('li').removeClass('active');

            $('#'+target_this+'').siblings('.tab-item').removeClass('active');
            $('#'+target_this+'').addClass('active');
            $('#'+target_this+'').get(0).slick.setPosition();

        })

    }
</script>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-94072929-1', 'auto');
  ga('send', 'pageview');

</script>


</body>
</html>