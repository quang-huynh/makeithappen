<?php
/**
 * Single page template (page-xuhuong.php)
 * Template Name: Xu Huong
 * @package WordPress
 * @subpackage ClickMedia-Make-It-Happen-template
 */

//QUERY XU HUONG LIST
$args = array(
    'numberposts' => 6,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'xu-huong-moi-nhat',
    'post_status' => 'publish',
    'suppress_filters' => true );

$xuhuong = get_posts( $args );

// var_dump($xuhuong);exit;

get_header(); // include header.php ?>



<main class="trend-page">
    <section class="banner-section" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <a href="#"><img src="<?php echo get_the_post_thumbnail_url() ?>" alt=""></a>
                </div>
            </div>
        </div>
        <!--<div class="banner-description">-->
        <!--<div class="container">-->
        <!--<div class="row">-->
        <!--<div class="col-xs-12">-->
        <!--<h1><a href="#">MASTER CAMO <br/> COLOR <br/> CORRECTING</a></h1>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
    </section>

    <section class="main-content main-content-custom">
        <div class="container">
            <div class="row">
                <div id="list-xuhuong" class="col-md-9 col-xs-12 left-content">

                    <div class="row user-box-full">
                        <div class="col-xs-12 news-title news-title-full">
                            <h1>XU HƯỚNG MỚI NHẤT</h1>
                        </div>
                    </div>


                    <?php 
                    	if (count($xuhuong) > 0) {
                    		$flag = 1;
                    		for ($i=0; $i < count($xuhuong); $i = $i+2) { 
                    ?>
                    	
		                    	<div class="row news-list">


			                        <div class="col-sm-5 col-xs-12 <?php if ($flag == 0) { echo 'pull-right';$flag=1;} else {$flag = 0;} ?> news-container">

			                            <div class="col-xs-12 news-img">
			                                <a href="<?php echo get_permalink($xuhuong[$i]->ID); ?>"><img src="<?php echo getFeaturedImageUrl($xuhuong[$i]->ID); ?>" alt=""></a>
			                            </div>

			                            <div class="col-xs-12 news-description full">
			                                <h3><?php echo $xuhuong[$i]->post_title ?></h3>
			                                <p><?php echo get_field('short_description', $xuhuong[$i]->ID); ?></p>
			                                <a href="<?php echo get_permalink($xuhuong[$i]->ID); ?>" class="view-more-btn">EXPLORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			                            </div>
			                        </div>

			                        <?php if (count($xuhuong) > 1 && $i + 1 < count($xuhuong)) { ?>
				                        <div class="col-sm-7 col-xs-12 news-container">
				                            <div class="col-sm-7 col-xs-12 news-img">
				                                <a href="<?php echo get_permalink($xuhuong[$i+1]->ID); ?>"><img src="<?php echo getFeaturedImageUrl($xuhuong[$i+1]->ID); ?>" alt=""></a>
				                            </div>

				                            <div class="news-description absolute">
				                                <h3><?php echo $xuhuong[$i+1]->post_title ?></h3>
				                                <p><?php echo get_field('short_description', $xuhuong[$i+1]->ID); ?></p>
				                                <a href="<?php echo get_permalink($xuhuong[$i+1]->ID); ?>" class="view-more-btn">EXPLORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
				                            </div>
				                        </div>
				                    <?php } ?>
			                    </div>

                    <?php 
                			} 
                		}
                	?>
                    
                    <?php if (count($xuhuong) > 0) { ?>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <a href="javascript:void(0)" id="load-more-xu-huong-button" class="view-more-btn has-border" data-index="6" data-format="left">LOAD MORE</a>
                            </div>
                        </div>
                    <?php } ?>

                </div>

                <div id="clone-xuhuong" style="display:none">

                	<div class="row news-list">


                        <div rel="item-1" class="col-sm-5 col-xs-12 news-container">

                            <div class="col-xs-12 news-img">
                                <a href="" rel="pemarlink-left-clone-xuhuong"><img rel="feature-img-left-clone-xuhuong" src="" alt=""></a>
                            </div>

                            <div class="col-xs-12 news-description full">
                                <h3 rel="title-left-clone-xuhuong"></h3>
                                <p rel="description-left-clone-xuhuong"></p>
                                <a href="" rel="pemarlink-left-clone-xuhuong" class="view-more-btn">EXPLORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>

                        <div rel="item-2" class="col-sm-7 col-xs-12  news-container">
                            <div class="col-sm-7 col-xs-12 news-img">
                                <a href="" rel="pemarlink-right-clone-xuhuong-2"><img rel="feature-img-right-clone-xuhuong" src="" alt=""></a>
                            </div>



                            <div class="news-description absolute">
                                <h3 rel="feature-img-right-clone-xuhuong"></h3>
                                <p rel="description-right-clone-xuhuong"></p>
                                <a href="" rel="pemarlink-right-clone-xuhuong-2" class="view-more-btn">EXPLORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                </div>


                <?php include_once( 'sidebar.php' ); ?>


            </div>
        </div>
    </section>
</main>




<?php // get_sidebar(); // include sidebar.php ?>
<?php get_footer(); // include footer.php ?>