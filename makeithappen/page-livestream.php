<?php
/**
 * Single page template (page-livestream.php)
 * Template Name: Live Stream
 * @package WordPress
 * @subpackage ClickMedia-Make-It-Happen-template
 */

//TOP LIVE STREAM LIST
$args = array(
    'numberposts' => 1,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'live-stream',
    'post_status' => 'publish',
    'suppress_filters' => true );

$livestream_top = get_posts( $args );

$args = array(
    'numberposts' => 10,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'live-stream',
    'post_status' => 'publish',
    'suppress_filters' => true );

$livestream = get_posts( $args );


get_header(); // include header.php ?>




<main class="black stream-page">
    <section class="video-section">
            <div class="container">
                <div class="row">
                    <h1 class="col-xs-12">LIVESTREAM</h1>

                    <div class="col-xs-12 video-box">
                        <img src="<?php echo getFeaturedImageUrl($livestream_top[0]->ID); ?>" alt="">
                        <iframe id="main_video" src="<?php echo get_field('link_youtube', $livestream_top[0]->ID); ?>" style="border:none;overflow:hidden" width="560" height="315" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                        <div class="video-description">
                            <div class="news-title">
                                <h3>WATCH NOW</h3>
                            </div>
                            <div class="video-content">
                                <h3><?php echo $livestream_top[0]->post_title ?></h3>
                                <p rel="limit-text"><?php echo get_field('short_description', $livestream_top[0]->ID); ?></p>
                                <div class="video-action">
                                    <ul>
                                        <li><i class="fa fa-heart-o" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-upload" aria-hidden="true"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="video-play-btn play-main-video"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
                    </div>
                </div>

                <div class="row video-list">
                    <div class="col-xs-12">
                        <div id="video-list-row" class="row video-list-row">


                        	<?php 
                                if (count($livestream) > 0) {
                                    for ($i=0; $i < count($livestream); $i++) { 
                                        if (get_field('status', $livestream[$i]->ID) == true) {
                            ?>

            	                            <div class="col-md-6 col-xs-12 video-item" data-url="<?php echo get_field('link_youtube', $livestream[$i]->ID); ?>">
            	                                <div class="col-sm-6 col-xs-12 video-img get-url-video">
            	                                    <img src="<?php echo getFeaturedImageUrl($livestream[$i]->ID); ?>" alt="">
            	                                    <div class="video-play-btn"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
            	                                </div>
            	                                <div class="col-sm-6 col-xs-12 video-description">
            	                                    <div class="news-title">
            	                                        <h3>WATCH NOW</h3>
            	                                    </div>
            	                                    <div class="video-content">
            	                                        <h3><?php echo $livestream[$i]->post_title ?></h3>
            	                                        <p rel="limit-text"><?php echo get_field('short_description', $livestream[$i]->ID); ?></p>
            	                                        <div class="video-action">
            	                                            <ul>
            	                                                <li><i class="fa fa-heart-o" aria-hidden="true"></i></li>
            	                                                <li><i class="fa fa-upload" aria-hidden="true"></i></li>
            	                                            </ul>
            	                                        </div>
            	                                    </div>
            	                                </div>
            	                            </div>

	                        <?php 
                                        }
                                    } 
                                }
                            ?>
                            
                        </div>


                    </div>

                    <div class="col-xs-12 text-center">
                        <a href="javascript:void(0)" id="load-more-live-stream-button" class="view-more-btn has-border" data-index="10">LOAD MORE</a>
                    </div>
                </div>


                <div id="clone-livestream" style="display:none">
                	<div rel="link-livestream" class="col-md-6 col-xs-12 video-item" data-url="">
                        <div class="col-sm-6 col-xs-12 video-img get-url-video">
                            <img rel="feature-image" src="" alt="">
                            <div class="video-play-btn"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
                        </div>
                        <div class="col-sm-6 col-xs-12 video-description">
                            <div class="news-title">
                                <h3>WATCH NOW</h3>
                            </div>
                            <div class="video-content">
                                <h3 rel="title"></h3>
                                <p rel="description"></p>
                                <div class="video-action">
                                    <ul>
                                        <li><i class="fa fa-heart-o" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-upload" aria-hidden="true"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </section>
</main>








<?php // get_sidebar(); // include sidebar.php ?>
<?php get_footer(); // include footer.php ?>

<script>
    $(document).on('ready', function () {
        videoAction();
    });
    
    function videoAction() {
        var get_url = '', set_height_iframe = 0;
        $('.video-box img').on('click touch', function () {
            var $this = $(this); set_height_iframe = $this.height();
            $this.closest('.video-box').find('.video-description, img, .video-play-btn').addClass('invisible-now');
            $('#main_video').height(set_height_iframe);
            $("#main_video")[0].src += "&autoplay=1";

            setTimeout(function () {
                $('#main_video').addClass('active');
            },1000);
        });


        $('.get-url-video').on('click touch', function () {
            var $this = $(this), position_scroll = parseInt($(".video-box").offset().top) - 100;
            get_url = $this.closest('.video-item').attr('data-url');

            $('.video-box').find('.video-description, img').addClass('invisible-now');
            $("#main_video").attr('src', get_url);
            $("#main_video")[0].src += "&autoplay=1";

            $(window).animate({scrollTop: position_scroll},500);

            setTimeout(function () {
                $('#main_video').addClass('active');
            },1000);



        });
    }
</script>