<?php
/**
 * Single page template (page-itgirl.php)
 * Template Name: IT Girl
 * @package WordPress
 * @subpackage ClickMedia-Make-It-Happen-template
 */

//TOP IT GIRL LIST
$args = array(
    'numberposts' => 1,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'dep-nhu-it-girl',
    'post_status' => 'publish',
    'suppress_filters' => true );

$itgirl_top = get_posts( $args );

//QUERY IT GIRL LIST
$args = array(
    'numberposts' => 6,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'dep-nhu-it-girl',
    'post_status' => 'publish',
    'suppress_filters' => true );

$itgirl = get_posts( $args );

// var_dump($itgirl);exit;

get_header(); // include header.php ?>



<main class="itgirl-page">
    <section class="banner-section" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>)">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <a href="#"><img src="<?php echo get_the_post_thumbnail_url() ?>" alt=""></a>
                </div>
            </div>
        </div>
        <!--<div class="banner-description">-->
        <!--<div class="container">-->
        <!--<div class="row">-->
        <!--<div class="col-xs-12">-->
        <!--<h1><a href="#">MASTER CAMO <br/> COLOR <br/> CORRECTING</a></h1>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
    </section>

    <section class="main-content main-content-custom">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-xs-12 left-content">

                    <div class="row user-box-full">
                        <div class="col-xs-12 news-title news-title-full">
                            <h1>ĐẸP NHƯ IT GIRL</h1>
                        </div>
                        <div class="col-xs-12 user-container-box">
                            <div class="col-sm-5 col-xs-12 user-container">
                                <div class="user-avatar" style="background-image: url(<?php echo getFeaturedImageUrl($itgirl_top[0]->ID); ?>)">
                                    <a href="<?php echo get_permalink($itgirl_top[0]->ID); ?>">
                                        <img src="<?php echo getFeaturedImageUrl($itgirl_top[0]->ID); ?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-7 col-xs-12 user-container-description">
                                <h3 class="user-container-description-title">MAKEUP <br/> TIPS</h3>
                                <div class="user-container-description-content">
                                    <h4><a href="<?php echo get_permalink($itgirl_top[0]->ID); ?>"><?php echo $itgirl_top[0]->post_title ?></a></h4>
                                    <p><?php echo get_field('short_description', $itgirl_top[0]->ID); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row user-box user-box-custom">
                        <div class="col-xs-12 title-custom title-custom-xlg">
                            <h1>MAKEUP <br/> MOVES FOR <br/> DAYS</h1>
                        </div>

                        <div id="itgirl-list" class="col-xs-12 user-container-list">

                            <?php 
                                if (count($itgirl) > 0) {
                                    for ($i=0; $i < count($itgirl); $i++) { 
                            ?>

                                <div class="col-xs-6 user-container">
                                    <div class="user-avatar" style="background-image: url(<?php echo getFeaturedImageUrl($itgirl[$i]->ID); ?>)">
                                        <a href="<?php echo get_permalink($itgirl[$i]->ID); ?>">
                                            <img src="<?php echo getFeaturedImageUrl($itgirl[$i]->ID); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="user-information">
                                        <div class="user-name">
                                            <h3><a href="<?php echo get_permalink($itgirl[$i]->ID); ?>"><?php echo $itgirl[$i]->post_title ?></a></h3>
                                        </div>
                                        <div class="user-job">
                                            <p><?php echo get_field('short_description', $itgirl[$i]->ID); ?></p>
                                        </div>
                                    </div>
                                </div>

                            <?php 
                                    } 
                                }
                            ?>


                        </div>

                        <?php 
                            if (count($itgirl) > 0) {
                        ?>
                            <div class="col-xs-12 text-center">
                                <a href="javascript:void(0)" id="load-more-it-girl-button" class="view-more-btn has-border" data-index="6">LOAD MORE</a>
                            </div>
                        <?php 
                                }
                            ?>

                    </div>


                    <div id="clone-itgirl" style="display:none">
                        <div class="col-xs-6 user-container">
                            <div class="user-avatar" rel="feature-image-bg">
                                <a href="" rel="pemarlink">
                                    <img rel="feature-image" src="" alt="">
                                </a>
                            </div>
                            <div class="user-information">
                                <div class="user-name">
                                    <h3 rel="title">
                                        <a href="" rel="permalink"></a>
                                    </h3>
                                </div>
                                <div class="user-job">
                                    <p rel="job-title"></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php include_once( 'sidebar.php' ); ?>
                
            </div>
        </div>
    </section>
</main>


<?php // get_sidebar(); // include sidebar.php ?>
<?php get_footer(); // include footer.php ?>