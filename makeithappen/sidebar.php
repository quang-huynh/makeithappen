<?php 


//QUERY DEP NHU IT GIRL
$args = array(
    'numberposts' => 5,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => array('xu-huong-moi-nhat', 'dep-nhu-it-girl'),
    'post_status' => 'publish',
    'suppress_filters' => true );

$sidebar_newest = get_posts( $args );


?>


<div class="col-md-3 col-xs-12 right-content">
    <div class="row">
    	<?php 
    		if(is_user_logged_in() == true) {
    	?>
    		<div class="col-xs-12 register-box">
    			<!-- <form action="http://test2.local/wp-login.php?action=logout&redirect_to=%2F"> -->
    				<a href="<?php echo wp_logout_url('/'); ?> ">
    					<button class="submit-btn">ĐĂNG XUẤT
    					<!-- <input type="submit" class="submit-btn" value="LOG OUT"> -->
    					</button>
    				</a>
    			<!-- </form> -->
    			<!-- <a href="<?php echo wp_logout_url('/'); ?> " class="submit-btn">
    				<button>Log Out</button>
    			</a> -->
    		</div>
    	
        <?php
    		} else {
    	?>
    		<div class="col-xs-12 register-box">
                <h3>ĐĂNG NHẬP / ĐĂNG KÝ</h3>

                <?php echo do_shortcode('[dm_login_form]'); ?>


                <!-- <form action="">
                    <div class="form-inner">
                        <input type="text" placeholder="Your name">
                        <div class="alert-custom">Vui lòng nhập tên</div>
                    </div>

                    <div class="form-inner">
                        <input type="text" placeholder="Your email-address">
                        <div class="alert-custom">Vui lòng nhập email</div>
                    </div>

                    <input type="submit" class="submit-btn" value="SIGN IN">
                </form> -->
                <span class="sign-note">Chưa có tài khoản ?</span>
                <a href="#" class="sign-up-btn target-popup" data-popup="register-popup">Đăng ký ngay</a>
            </div>
    		
    	<?php
    		}
    	?>
        
    </div>

    <div class="row">
        <div class="col-xs-12 events-box">
            <h3 class="title-sidebar">SỰ KIỆN</h3>
            <!-- <select name="sort-month">
                <option value="1">January</option>
                <option value="2">Frebruary</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select> -->

            <!-- <div class="events-list">
                <div class="col-md-12 col-sm-3 col-xs-6 events-item">
                    <a href="#"><img src="<?php bloginfo('template_url')?>/images/events_03.jpg" alt=""></a>
                </div>
                <div class="col-md-12 col-sm-3 col-xs-6 events-item">
                    <a href="#"><img src="<?php bloginfo('template_url')?>/images/events_03.jpg" alt=""></a>
                </div>
                <div class="col-md-12 col-sm-3 col-xs-6 events-item">
                    <a href="#"><img src="<?php bloginfo('template_url')?>/images/events_03.jpg" alt=""></a>
                </div>
                <div class="col-md-12 col-sm-3 col-xs-6 events-item">
                    <a href="#"><img src="<?php bloginfo('template_url')?>/images/events_03.jpg" alt=""></a>
                </div>
            </div> -->

            <ul>

                <li>
                    <h5>21/03/2017: Event MAKE IT HAPPEN</h5>
                </li>

            </ul>

            <!-- <a href="#" class="view-more-btn">SEE ALL</a> -->
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 recent-post">
            <h3 class="title-sidebar">BÀI VIẾT GẦN ĐÂY</h3>

            <div class="recent-post-list">
                <?php foreach ($sidebar_newest as $key => $item) { ?>
                    <div class="recent-post-item">
                        <h3><a href="<?php echo get_permalink($item->ID); ?>"><?php echo $item->post_title ?></a></h3>
                        <p rel="limit-text"><?php echo get_field('short_description', $item->ID); ?></p>
                        <a href="<?php echo get_permalink($item->ID); ?>">View more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>