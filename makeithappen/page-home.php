<?php
/**
 * Single page template (page-home.php)
 * Template Name: Homepage
 * @package WordPress
 * @subpackage ClickMedia-Make-It-Happen-template
 */


//QUERY XU HUONG MOI NHAT
$args = array(
    'numberposts' => 1,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'xu-huong-moi-nhat',
    'post_status' => 'publish',
    'suppress_filters' => true );

$xuhuongmoinhat = get_posts( $args );


//QUERY DEP NHU IT GIRL
$args = array(
    'numberposts' => 1,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'dep-nhu-it-girl',
    'post_status' => 'publish',
    'suppress_filters' => true );

$depnhuitgirl = get_posts( $args );


//QUERY TÔI ĐÃ MAKE IT HAPPEN
$args = array(
    'numberposts' => 6,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'toi-da-mih',
    'post_status' => 'publish',
    'suppress_filters' => true );

$toidamih = get_posts( $args );


//QUERY BÀI MỚI NHẤT SLIDER
$args = array(
    'numberposts' => 5,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => array('xu-huong-moi-nhat', 'dep-nhu-it-girl'),
    'post_status' => 'publish',
    'suppress_filters' => true );

$slider_newest = get_posts( $args );


//QUERY DEP NHU IT GIRL
$args = array(
    'numberposts' => 5,
    'offset' => 0,
    'meta_key' => 'wpb_post_views_count',
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'post_type' => array('xu-huong-moi-nhat', 'dep-nhu-it-girl'),
    'post_status' => 'publish',
    'suppress_filters' => true );

$slider_popular = get_posts( $args );



//var_dump(get_field('banner_slider'));exit;






get_header(); // include header.php ?>

				<main class="home-page">





				    <section class="banner-section-slider">
				    	<?php foreach (get_field('banner_slider') as $key => $item) { ?>
					        <div class="banner-slider-item">
					            <a href="<?php echo $item['link']; ?>"><img src="<?php echo $item['images']; ?>" alt=""></a>
					        </div>

				        <?php } ?>

				    </section>



				    <section class="main-content">
				        <div class="container">
				            <div class="row">
				                <div class="col-md-9 col-xs-12 left-content">
				                    <div class="row">
				                        <div class="col-xs-12 nav-filter">
				                            <ul>
				                                <li class="active" data-tab="lasted"><a href="#">BÀI MỚI NHẤT</a></li>
				                                <li data-tab="best-view"><a href="#">XEM NHIỀU NHẤT</a></li>
				                            </ul>
				                        </div>
				                    </div>

				                    <div class="row" style="position: relative">
				                        <div class="news-slider tab-item active" id="lasted">
				                        	<?php foreach ($slider_newest as $key => $item) { ?>
					                            <div class="news-container">
					                                <div class="news-img" style="background-image: url(<?php echo getFeaturedImageUrl($item->ID); ?>)">
					                                    <!-- <a href="<?php echo get_permalink($item->ID); ?>"><img src="<?php echo getFeaturedImageUrl($item->ID); ?>" alt=""></a> -->
					                                    <a href="<?php echo get_permalink($item->ID); ?>">
					                                    	<img src="<?php echo getFeaturedImageUrl($item->ID); ?>">
					                                    </a>
					                                </div>

					                                <div class="news-description full absolute">
					                                    <h3><?php echo $item->post_title ?></h3>
					                                    <p><?php echo get_field('short_description', $item->ID); ?></p>
					                                    <a href="<?php echo get_permalink($item->ID); ?>" class="view-more-btn">Khám phá <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					                                </div>
					                            </div>
					                        <?php } ?>
				                        </div>

				                        <div class="news-slider tab-item" id="best-view">
				                            <?php foreach ($slider_popular as $key => $item) { ?>
					                            <div class="news-container">
					                                <div class="news-img" style="background-image: url(<?php echo getFeaturedImageUrl($item->ID); ?>)">
					                                    <!-- <a href="<?php echo get_permalink($item->ID); ?>"><img src="<?php echo getFeaturedImageUrl($item->ID); ?>" alt=""></a> -->
					                                    <a href="<?php echo get_permalink($item->ID); ?>">
					                                    	<img src="<?php echo getFeaturedImageUrl($item->ID); ?>">
					                                    </a>
					                                </div>

					                                <div class="news-description full absolute">
					                                    <h3><?php echo $item->post_title ?></h3>
					                                    <p><?php echo get_field('short_description', $item->ID); ?></p>
					                                    <a href="<?php echo get_permalink($item->ID); ?>" class="view-more-btn">Khám phá <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					                                </div>
					                            </div>
					                        <?php } ?>
				                        </div>

				                    </div>

				                    <div class="row">
				                        <div class="col-sm-5 col-xs-12 news-container">
				                            <div class="col-xs-12 news-title">
				                                <h3>XU HƯỚNG MỚI NHẤT</h3>
				                            </div>

				                            <div class="col-xs-12 news-img">
				                                <a href="<?php echo get_permalink($xuhuongmoinhat[0]->ID); ?>"><img src="<?php echo getFeaturedImageUrl($xuhuongmoinhat[0]->ID); ?>" alt=""></a>
				                            </div>

				                            <div class="col-xs-12 news-description full">
			                                	<h3>
			                                		<?php echo $xuhuongmoinhat[0]->post_title ?>
			                                	</h3>
				                                <p><?php echo get_field('short_description', $xuhuongmoinhat[0]->ID); ?></p>
				                                <a href="<?php echo get_permalink($xuhuongmoinhat[0]->ID); ?>" class="view-more-btn">Khám phá <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
				                            </div>
				                        </div>

				                        <div class="col-sm-7 col-xs-12  news-container">
				                            <div class="col-sm-5 col-xs-12 pull-right news-title has-padding">
				                                <h3 style="white-space: nowrap;">ĐẸP NHƯ IT GIRL</h3>
				                            </div>
				                            <div class="col-sm-7 col-xs-12 news-img">
				                                <a href="<?php echo get_permalink($depnhuitgirl[0]->ID); ?>"><img src="<?php echo getFeaturedImageUrl($depnhuitgirl[0]->ID); ?>" alt=""></a>
				                            </div>



				                            <div class="news-description absolute">
			                                	<h3>
			                                		<?php echo $depnhuitgirl[0]->post_title ?>
			                                	</h3>
				                                <p><?php echo get_field('short_description', $xuhuongmoinhat[0]->ID); ?></p>
				                                <a href="<?php echo get_permalink($depnhuitgirl[0]->ID); ?>" class="view-more-btn">Khám phá <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
				                            </div>
				                        </div>
				                    </div>

				                    <div class="row">
				                        <div class="col-xs-12 title-custom">
				                            <h1>TÔI ĐÃ "MAKE IT HAPPEN"</h1>
				                        </div>
				                    </div>

				                    <div class="row user-box q-toi-da-mih">
				                    	<?php foreach ($toidamih as $key => $item) { ?>
				                            <div class="col-sm-4 col-xs-6 user-container">
					                            <div class="user-avatar" style="background-image: url(<?php echo getFeaturedImageUrl($item->ID); ?>)">
					                            	<a href="<?php echo get_permalink($item->ID); ?>">
					                                	<img src="<?php echo getFeaturedImageUrl($item->ID); ?>" alt="">
					                                </a>
					                            </div>
					                            <div class="user-information">
					                                <div class="user-name">
					                                    <h3><a href="<?php echo get_permalink($item->ID); ?>"><?php echo get_field('title', $item->ID); ?></a></h3>
					                                </div>
					                                <div class="user-job">
					                                    <p><?php echo get_field('job_title', $item->ID); ?></p>
					                                </div>
					                            </div>
					                        </div>
				                        <?php } ?>

				                    </div>


				                    <!--

				                    <div class="row our-fans">
				                        <div class="title-our-fans">
				                            <h3>FROM OUR FANS</h3>
				                            <p>Join the lagest community of success peoples</p>
				                        </div>

				                        <div class="slider-fans-box">
				                            <div class="slider-fans-item"><a href="#"><img src="<?php bloginfo('template_url')?>/images/fan-img_03.jpg" alt=""></a></div>
				                            <div class="slider-fans-item"><a href="#"><img src="<?php bloginfo('template_url')?>/images/fan-img_05.jpg" alt=""></a></div>
				                            <div class="slider-fans-item"><a href="#"><img src="<?php bloginfo('template_url')?>/images/fan-img_07.jpg" alt=""></a></div>
				                            <div class="slider-fans-item"><a href="#"><img src="<?php bloginfo('template_url')?>/images/fan-img_09.jpg" alt=""></a></div>
				                            <div class="slider-fans-item"><a href="#"><img src="<?php bloginfo('template_url')?>/images/fan-img_03.jpg" alt=""></a></div>
				                            <div class="slider-fans-item"><a href="#"><img src="<?php bloginfo('template_url')?>/images/fan-img_05.jpg" alt=""></a></div>
				                            <div class="slider-fans-item"><a href="#"><img src="<?php bloginfo('template_url')?>/images/fan-img_07.jpg" alt=""></a></div>
				                            <div class="slider-fans-item"><a href="#"><img src="<?php bloginfo('template_url')?>/images/fan-img_09.jpg" alt=""></a></div>
				                        </div>

				                        <div class="view-more-btn-box text-center">
				                            <a href="#" class="view-more-btn">Khám phá MORE</a>
				                        </div>
				                    </div>

				                    <div class="row share-box">
				                        <div class="col-sm-4 share-step">
				                            <div class="step-number">
				                                <img src="<?php bloginfo('template_url')?>/images/step_01.png" alt="">
				                            </div>
				                            <div class="step-description">
				                                <p>UPLOAD FILDE</p>
				                            </div>
				                            <div class="step-action">
				                                <button type="button" class="upload-file-btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
				                            </div>
				                        </div>
				                        <div class="col-sm-4 share-step">
				                            <div class="step-number">
				                                <img src="<?php bloginfo('template_url')?>/images/step_02.png" alt="">
				                            </div>
				                            <div class="step-description">
				                                <p>TELL US WHAT YOU MAKE</p>
				                            </div>
				                            <div class="step-action">
				                                <textarea name="your-message"></textarea>
				                            </div>
				                        </div>
				                        <div class="col-sm-4 share-step">
				                            <div class="step-number">
				                                <img src="<?php bloginfo('template_url')?>/images/step_03.png" alt="">
				                            </div>
				                            <div class="step-description">
				                                <p>SHARE</p>
				                            </div>
				                            <div class="step-action">
				                                <div class="agree">
				                                    <input type="checkbox">
				                                    <span>I agree to the Terms & Conditions</span>
				                                </div>

				                                <button type="button" class="share-story-btn">SHARE YOUR STORY</button>
				                            </div>
				                        </div>
				                    </div>


				                	-->

				                    <div class="row my-style">
				                        <!-- <div class="col-xs-12 title-custom">
				                            <h1>PHONG CÁCH NY CỦA TÔI</h1>
				                        </div> -->

				                        <!-- <div class="col-sm-3 col-xs-6 my-style-block">
				                            <div class="my-style-img" style="background-image: url(<?php bloginfo('template_url')?>/images/style_06.jpg)">
				                                <a href="#"><img src="<?php bloginfo('template_url')?>/images/style_06.jpg" alt=""></a>
				                            </div>
				                            <div class="my-style-info">
				                                <h3><a href="#">ENYA MOMMSEN</a></h3>
				                            </div>
				                        </div> -->


				                    </div>

				                </div>


				                <?php include_once( 'sidebar.php' ); ?>



				            </div>
				        </div>
				    </section>
				</main>
			
			

<?php // get_sidebar(); // include sidebar.php ?>
<?php get_footer(); // include footer.php ?>