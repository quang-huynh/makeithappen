$(document).on('ready', function () {

    function validateRegister() {
        var get_username = $('#inner_username').val(),
            get_mail = $('#inner_mail').val(),
            get_password = $('#inner_password').val(),
            get_repassword = $('#inner_repassword').val(),
            get_firstname = $('#inner_firstname').val(),
            get_lastname = $('#inner_lastname').val(),
            get_phone = $('#inner_phone').val(),
            get_gender = $('#inner_gender').val(),
            get_dob = $('#inner_dob').val(),
            get_address = $('#inner_address').val(),
            checked = $('#agree_rules').prop('checked');

        var data = [get_username, get_mail, get_password, get_firstname, get_lastname, get_phone, get_gender, get_dob, get_address];

        if(get_username == '' || get_username.length <= 3) {
            $('.alert-custom').hide();
            $('#inner_username').parent().find('.alert-custom').show();

            return false;
        }

        if (get_username.indexOf(' ') > -1) {
            $('.alert-custom').hide();
            $('#inner_username').parent().find('.alert-custom').show();

            return false;
        }

        if(!validateEmail(get_mail)) {
            $('.alert-custom').hide();
            $('#inner_mail').parent().find('.alert-custom').show();
            return false;
        }


        if (get_mail.indexOf(' ') > -1) {
            $('.alert-custom').hide();
            $('#inner_username').parent().find('.alert-custom').show();

            return false;
        }

        if(get_password == '') {
            $('.alert-custom').hide();
            $('#inner_password').parent().find('.alert-custom').show();
            return false;
        }

        if(get_repassword == '') {
            $('.alert-custom').hide();
            $('#inner_repassword').parent().find('.alert-custom').show();
            return false;
        }

        if(get_password !== get_repassword) {
            $('.alert-custom').hide();
            $('#inner_repassword').parent().find('.alert-custom').show();
            return false;
        }

        if(get_firstname == '') {
            $('.alert-custom').hide();
            $('#inner_firstname').parent().find('.alert-custom').show();
            return false;
        }

        if(get_lastname == '') {
            $('.alert-custom').hide();
            $('#inner_lastname').parent().find('.alert-custom').show();
            return false;
        }

        if(!isVNMobileNumber(get_phone)) {
            $('.alert-custom').hide();
            $('#inner_phone').parent().find('.alert-custom').show();
            return false;
        }

        if(get_gender == 0) {
            $('.alert-custom').hide();
            $('#inner_gender').parent().find('.alert-custom').show();
            return false;
        }

        if(!validateDOB(get_dob)) {
            $('.alert-custom').hide();
            $('#inner_dob').parent().find('.alert-custom').show();
            return false;
        }

        if(get_address == '') {
            $('.alert-custom').hide();
            $('#inner_address').parent().find('.alert-custom').show();
            return false;
        }

        if(!checked) {
            $('.alert-custom').hide();
            $('#agree_rules').parent().find('.alert-custom').show();
            return false;
        }


        $('.alert-custom').hide();

        $('#inner_username,#inner_firstname,#inner_lastname, #inner_mail, #inner_password, #inner_repassword, #inner_phone, #inner_dob, #inner_address').val('');
        $('#inner_gender').val(0);
        
        return data;
        
        // console.log(get_name,get_mail,get_phone,get_gender,get_dob,get_address);
    }



    // Register validate
    $('.send-form').on('click touch', function () {
        var that = $( this );
        var data = validateRegister();
        var data_post = {'action': 'create_account', 'username' : data[0], 'email' : data[1], 'password' : data[2], 'first_name' : data[3], 'last_name' : data[4], 'phone' : data[5], 'gender' : data[6], 'birthday' : data[7], 'address' : data[8]}
        if (data !== false) {
            $.ajax({
                 url: ajaxurl,
                 type: 'POST',
                 dataType: 'json',
                 data: data_post
            }).done(function(result) {
                if (result.code == 1) {
                    that.addClass( "btn-success" );
                    $('#mess-response').empty().append('Tạo tài khoản thành công !').focus().fadeIn();
                    $('#inner_username,#inner_firstname,#inner_lastname, #inner_mail,#innter_password,#inner_repassword #inner_phone, #inner_dob, #inner_address').val('');
                    $('#inner_gender').val(0);
                }
                if (result.code == 0) {
                    $('#mess-response').empty().append(result.mess).focus().fadeIn();
                }
                    
            });
            // $.post(ajaxurl, data_post, function(response){
            //     alert('Du lieu duoc tra ve tu server: ' + response);
            // });
        }
    });

    $.each($('[rel="limit-text"]'), function(index, val) {
        var txt = $(val).text();
        if(txt.length > 155)
            $(val).text(txt.substring(0,200) + '.....');
    });


    $.each($('.user-container .user-job p'), function(index, val) {
        var txt = $(val).text();
        if(txt.length > 155)
            $(val).text(txt.substring(0,200) + '.....');
    });


    $.each($('.news-container .news-description.full p'), function(index, val) {
        var txt = $(val).text();
        if(txt.length > 155)
            $(val).text(txt.substring(0,200) + '.....');
    });


    $.each($('.video-description p'), function(index, val) {
        var txt = $(val).text();
        if(txt.length > 155)
            $(val).text(txt.substring(0,200) + '.....');
    });


    


    



    $('body').on('click', '#load-more-xu-huong-button', function() {

        var that = $(this);
        var clone_that = that.parent().clone();
        var index = that.data('index');
        var format = that.data('format');

        $.ajax({
            url: ajaxurl,
            type: 'POST',
            dataType: 'json',
            data: {'action': 'load_xuhuong', 'index' : index},
        })
        .done(function(result) {

            that.parent().remove();

            if (result.code == '1' || result.code == 'end') {
                var clone = $('#clone-xuhuong').find('.news-list').clone();

                $.each(clone.find('[rel="pemarlink-left-clone-xuhuong"]'), function(index, val) {
                    $(val).attr('href', result.permalink[0]);
                });
                clone.find('[rel="feature-img-left-clone-xuhuong"]').attr('src', result.linkFeatureImg[0]);
                clone.find('[rel="title-left-clone-xuhuong"]').text(result.items[0].post_title);
                clone.find('[rel="description-left-clone-xuhuong"]').text(result.descriptionCustomField[0]);
                if (result.items.length == 2) {
                    $.each(clone.find('[rel="pemarlink-right-clone-xuhuong"]'), function(index, val) {
                        $(val).attr('href', result.permalink[1]);
                    });
                    clone.find('[rel="feature-img-right-clone-xuhuong"]').attr('src', result.linkFeatureImg[1]);
                    clone.find('[rel="title-right-clone-xuhuong"]').text(result.items[1].post_title);
                    clone.find('[rel="description-right-clone-xuhuong"]').text(result.descriptionCustomField[1]);
                } else {
                    clone.find('[rel="item-2"]').remove();
                }


                if (format == "left") {
                    clone.find('[rel="item-1"]').addClass('pull_right');
                    that.attr('data-format',"right");
                    that.data('format',"right");
                } else {
                    that.attr('data-format',"left");
                    that.data('format',"left");
                }

                console.log(clone);

                $('#list-xuhuong').append(clone);
                

                if (result.code != 'end' && result.code != '0') {
                    $('#list-xuhuong').append(clone_that);

                }

                if (result.code == '1' || result.code == 'end') {
                    clone_that.find('#load-more-xu-huong-button').attr('data-index',result.index);
                    clone_that.find('#load-more-xu-huong-button').data('index', result.index);
                }

            }

        })
        .fail(function() {
            
        })
    });




    $('body').on('click', '#load-more-it-girl-button', function() {

        var that = $(this);
        var clone_that = that.parent().clone();
        var index = that.data('index');

        $.ajax({
            url: ajaxurl,
            type: 'POST',
            dataType: 'json',
            data: {'action': 'load_itgirl', 'index' : index},
        })
        .done(function(result) {

            that.parent().remove();

            if (result.code == '1' || result.code == 'end') {

                $.each(result.items, function(index, ob) {
                    var clone = $('#clone-itgirl').find('.user-container').clone();

                    $.each(clone.find('[rel="pemarlink"]'), function(index, val) {
                        $(val).attr('href', result.permalink[index]);
                    });
                    clone.find('[rel="feature-image"]').attr('src', result.linkFeatureImg[index]);
                    clone.find('[rel="feature-image-bg"]').css('background-image', 'url(' + result.linkFeatureImg[index] + ')');
                    feature-image-bg
                    clone.find('[rel="title"] a').text(ob.post_title);
                    clone.find('[rel="job-title"]').text(result.jobTitle[index]);


                    $('#itgirl-list').append(clone);
                });

                if (result.code != 'end' && result.code != '0') {
                    $('#itgirl-list').append(clone_that);

                }

                if (result.code == '1' || result.code == 'end') {
                    clone_that.find('#load-more-it-girl-button').attr('data-index',result.index);
                    clone_that.find('#load-more-it-girl-button').data('index', result.index);
                }

            }

        })
        .fail(function() {
            
        })
    });



    $('body').on('click', '#load-more-live-stream-button', function() {

        var that = $(this);
        // var clone_that = that.parent().clone();
        var index = that.data('index');

        $.ajax({
            url: ajaxurl,
            type: 'POST',
            dataType: 'json',
            data: {'action': 'load_livestream', 'index' : index},
        })
        .done(function(result) {
            if (result.code == '1' || result.code == 'end') {

                $.each(result.items, function(index, ob) {
                    if (ob.post_title.length != 0) {
                        var clone = $('#clone-livestream').find('.video-item').clone();

                        $.each(clone.find('[rel="pemarlink"]'), function(index, val) {
                            $(val).attr('href', result.permalink[index]);
                        });
                        clone.find('[rel="feature-image"]').attr('src', result.linkFeatureImg[index]);
                        clone.find('[rel="title"]').text(ob.post_title);
                        clone.find('[rel="description"]').text(result.descriptionCustomField[index]);
                        clone.attr('data-url',result.link_livestream[index]);
                        clone.data('url',result.link_livestream[index]);


                        $('#video-list-row').append(clone);
                    }
                        
                });

                if (result.code != 'end' && result.code != '0') {
                    // $('#video-list-row').append(clone_that);
                }

                if (result.code == '1' || result.code == 'end') {
                    that.attr('data-index',result.index);
                    that.data('index', result.index);
                }


                videoAction();

            } else {
                that.remove();
            }

        })
        .fail(function() {
            
        })
    });


    // $('.video-play-btn').on('click', function(){
    //     $('.video-box .play-main-video').fadeOut();
    //     // $('.video-box img').css('opacity', 0);
    //     $('.video-box img').fadeOut();
    //     $('iframe#main_video').fadeIn();
    //     setTimeout(function(){
    //         $.scrollTo($('iframe#main_video'), 1000);
    //     }, 500);
        
    // });


    // $('.video-img').on('click', function(){
    //     $('.video-box .play-main-video').fadeOut();
    //     // $('.video-box img').css('opacity', 0);
    //     $('.video-box img').fadeOut();
    //     $('iframe#main_video').fadeIn();

    //     setTimeout(function(){
    //         $.scrollTo($('iframe#main_video'), 1000);
    //     }, 500);
    // });

    // videoAction();
    // function videoAction() {
    //     get_url = '';
    //     $('.video-box img').on('click touch', function () {
    //         var $this = $(this);
    //        $('#main_video').addClass('active');
    //        $this.closest('.video-box').find('.video-description, img, .video-play-btn').addClass('invisible-now');
    //     });


    //     $('.get-url-video').on('click touch', function () {
    //         var $this = $(this);
    //         get_url = $this.closest('.video-item').attr('data-url');

    //         $('.video-box').find('.video-description, img').addClass('invisible-now');
    //         $('#main_video').addClass('active');
    //         $("#main_video").attr('src', get_url);



    //     });
    // }

    

});