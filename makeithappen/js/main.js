$(document).on('ready', function () {
    menuSetting();
    backTop();
    popupSetting();

    //Close all popup
    $('.overlay').on('click touch', function () {
        closeAll();
    });

    $(window).on('keydown', function (event) {
        if(event.keyCode == 27) {
            closeAll();
        }
    });
    // -------

    // Datetimepicker
    $('#inner_dob').datetimepicker({
        format: 'L',
        viewMode: 'years'
    });

    // Register validate
    $('.send-form').on('click touch', function () {
<<<<<<< HEAD:makeithappen/js/main.js
        //validateRegister();
=======
        // validateRegister();
>>>>>>> c32722012cde671d1218cbfb388b819de8e9bcdc:www/js/main.js
    });

    // callSuccess();
});

var check_mobile = false;

function menuSetting() {
    $('.menu-hambergur').on('click touch', function () {
        $('.popup, .search-box').removeClass('active');
        $(this).toggleClass('active');
        $('header').toggleClass('mobile-menu-active');
        $('main').toggleClass('mobile-menu-active');
    });

    $('.search').on('click touch', function () {
        $('.popup, .menu-hambergur').removeClass('active');
        $('header, main').removeClass('mobile-menu-active');
        $('.search-box').toggleClass('active');
    });

    $('main').on('click touch', function () {
        $('.menu-hambergur').removeClass('active');
        $('header').removeClass('mobile-menu-active');
        $('main').removeClass('mobile-menu-active');
    });

    $(window).on('resize load', function () {
       if($(window).width() < 768) {
           check_mobile = true;
       } else {
           check_mobile = false;
       }
    });

    $(window).on('scroll', function () {
        if(check_mobile) {
            $('body').removeClass('fixed-menu-desktop');
            if($(window).scrollTop() > 40) {
                $('body').addClass('fixed-menu-mobile');

                if($(window).scrollTop() > 41) {
                    $('body').addClass('active-menu');
                }
            } else {
                $('body').removeClass('fixed-menu-mobile active-menu');
            }
        } else {
            $('body').removeClass('fixed-menu-mobile');
            if($(window).scrollTop() > 93) {
                $('body').addClass('fixed-menu-desktop');

                if($(window).scrollTop() > 94) {
                    $('body').addClass('active-menu');
                }
            } else {
                $('body').removeClass('fixed-menu-desktop active-menu');
            }
        }

    });
}

function backTop() {
    $(window).on('scroll', function () {
        if($(window).scrollTop() > $(window).height()) {
            $('.back-to-top').addClass('active');
        } else {
            $('.back-to-top').removeClass('active');
        }
    });

    $('.back-to-top').on('click touch', function () {
        $(window).animate({scrollTop: 0}, 500);
    });
}

function popupSetting() {
    $('.target-popup').on('click touch', function (event) {
        var $this = $(this), target_id = $this.attr('data-popup');

        if($this.is('a')) {
            event.preventDefault();
        }

        $('.popup, .search-box, .menu-hambergur').removeClass('active');
        $('header, main').removeClass('mobile-menu-active');
        $('#'+target_id+', .overlay').addClass('active');
    });

    $('.close-popup').on('click touch', function () {
       $(this).closest('.popup').removeClass('active');
       $('.overlay').removeClass('active');
    });
}

function closeAll() {
    $('.popup, .overlay, .search-box').removeClass('active');
    $('.menu-hambergur').removeClass('active');
    $('header').removeClass('mobile-menu-active');
    $('main').removeClass('mobile-menu-active');
}

function validateRegister() {
    var get_name = $('#inner_name').val(),
        get_mail = $('#inner_mail').val(),
        get_phone = $('#inner_phone').val(),
        get_gender = $('#inner_gender').val(),
        get_dob = $('#inner_dob').val(),
        get_address = $('#inner_address').val(),
        checked = $('#agree_rules').prop('checked');

    if(get_name == '') {
        $('.alert-custom').hide();
        $('#inner_name').parent().find('.alert-custom').show();
        return false;
    }

    if(!validateEmail(get_mail)) {
        $('.alert-custom').hide();
        $('#inner_mail').parent().find('.alert-custom').show();
        return false;
    }

    if(!isVNMobileNumber(get_phone)) {
        $('.alert-custom').hide();
        $('#inner_phone').parent().find('.alert-custom').show();
        return false;
    }

    if(get_gender == 0) {
        $('.alert-custom').hide();
        $('#inner_gender').parent().find('.alert-custom').show();
        return false;
    }

    if(!validateDOB(get_dob)) {
        $('.alert-custom').hide();
        $('#inner_dob').parent().find('.alert-custom').show();
        return false;
    }

    if(get_address == '') {
        $('.alert-custom').hide();
        $('#inner_address').parent().find('.alert-custom').show();
        return false;
    }

    if(!checked) {
        $('.alert-custom').hide();
        $('#agree_rules').parent().find('.alert-custom').show();
        return false;
    }


    $('.alert-custom').hide();
    $('#inner_name, #inner_mail, #inner_phone, #inner_dob, #inner_address').val('');
    $('#inner_gender').val(0);
    console.log(get_name,get_mail,get_phone,get_gender,get_dob,get_address);
}

// check phone number value
function isVNMobileNumber(number) {
    var flag = false;
    if (!$.isNumeric(number)) return false;
    $.each(number.split("") ,function(index, el) {
        if (index == 0) {
            if (el != 0) return false;
        }
        else {
            if (index == 1) {
                if (el == 9 || el==8) {
                    if (number.length == 10) flag = true;
                } else if (el == '1') {
                    if (number.length == 11) flag = true;
                } else {
                    // return false;
                }
            }
            return;
        }

    });
    return flag;
}

// check date of birth value
function validateDOB(number) {
    var pattern = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    if (number == null || number == "" || !pattern.test(number)) {
        return false;
    }
    else {
        return true
    }
}

// check email value
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function callSuccess() {
    $('.popup').removeClass('active');
    $('.success, .overlay').addClass('active');
    setTimeout(function () {
        $('.success, .overlay').removeClass('active');
    }, 2000);
}