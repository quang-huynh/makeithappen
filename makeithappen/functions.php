<?php
/**
 * ClickMedia - Make It Happen functions and definitions
 *
 * @package WordPress
 * @subpackage ClickMedia - Make It Happen
 */

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'widgets' );

require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/insert_data.php';

function xuhuongmoinhat_init() {
	$args = array(
	  'label' => 'Xu Hướng Mới Nhất',
	    'public' => true,
	    'show_ui' => true,
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'rewrite' => array('slug' => 'xu-huong-moi-nhat'),
	    'query_var' => true,
	    'menu_icon' => 'dashicons-admin-page',
	    'supports' => array(
	        'title',
	        'editor',
	        'custom-fields',
	        'thumbnail')
	    );
	register_post_type( 'xu-huong-moi-nhat', $args );
}

function depnhuitgirl_init() {
	$args = array(
	  'label' => 'Đẹp Như IT GIRL',
	    'public' => true,
	    'show_ui' => true,
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'rewrite' => array('slug' => 'dep-nhu-it-girl'),
	    'query_var' => true,
	    'menu_icon' => 'dashicons-admin-page',
	    'supports' => array(
	        'title',
	        'editor',
	        'custom-fields',
	        'thumbnail')
	    );
	register_post_type( 'dep-nhu-it-girl', $args );
}

function toidamih_init() {
	$args = array(
	  'label' => 'TÔI ĐÃ MIH',
	    'public' => true,
	    'show_ui' => true,
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'rewrite' => array('slug' => 'toi-da-mih'),
	    'query_var' => true,
	    'menu_icon' => 'dashicons-admin-page',
	    'supports' => array(
	        'title',
	        'editor',
	        'custom-fields',
	        'thumbnail')
	    );
	register_post_type( 'toi-da-mih', $args );
}

function phongcachny_init() {
	$args = array(
	  'label' => 'Phong Cách NY',
	    'public' => true,
	    'show_ui' => true,
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'rewrite' => array('slug' => 'phong-cach-ny'),
	    'query_var' => true,
	    'menu_icon' => 'dashicons-admin-page',
	    'supports' => array(
	        'title',
	        'editor',
	        'custom-fields',
	        'thumbnail')
	    );
	register_post_type( 'phong-cach-ny', $args );
}

function livestream_init() {
	$args = array(
	  'label' => 'Live Stream',
	    'public' => true,
	    'show_ui' => true,
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'rewrite' => array('slug' => 'live-stream'),
	    'query_var' => true,
	    'menu_icon' => 'dashicons-admin-page',
	    'supports' => array(
	        'title',
	        'editor',
	        'custom-fields',
	        'thumbnail')
	    );
	register_post_type( 'live-stream', $args );
}


add_action( 'init', 'xuhuongmoinhat_init' );
add_action( 'init', 'depnhuitgirl_init' );
add_action( 'init', 'toidamih_init' );
add_action( 'init', 'phongcachny_init' );
add_action( 'init', 'livestream_init' );


add_action( 'init', 'gp_register_taxonomy_for_object_type' );
function gp_register_taxonomy_for_object_type() {
    register_taxonomy_for_object_type( 'post_tag', 'xu-huong-moi-nhat' );
    register_taxonomy_for_object_type( 'post_tag', 'dep-nhu-it-girl' );
    register_taxonomy_for_object_type( 'post_tag', 'toi-da-make-it-happen' );
    register_taxonomy_for_object_type( 'post_tag', 'phong-cach-ny' );
    register_taxonomy_for_object_type( 'post_tag', 'live-stream' );
};



function getFeaturedImageUrl($postId) {
    $postImage = wp_get_attachment_image_src( 
                    get_post_thumbnail_id($postId), 
                    'single-post-thumbnail' 
                ); 
    if($postImage) {
        return $postImage[0];
    }
    
    return false;
}

function getImageFromId($imageId, $size = 'full') {
    $image = wp_get_attachment_image_src( 
                   $imageId, $size
                ); 
    if($image) {
        return $image[0];
    }

    return false;
}


add_image_size( 'custom-thumnail-size', 300, 400, array( 'center', 'center' ) );



function load_xuhuong_ajax_callback() {

	$index = ( isset($_POST['index']) ? $_POST['index'] : 0 );

    $args = array(
	    'numberposts' => 2,
	    'offset' => $index,
	    'orderby' => 'post_date',
	    'order' => 'DESC',
	    'post_type' => 'xu-huong-moi-nhat',
	    'post_status' => 'publish',
	    'suppress_filters' => true );

	$xuhuong_ajax = get_posts( $args );

	if (count($xuhuong_ajax) == 2) {
		$index += 2;
		$code = '1';
	} elseif (count($xuhuong_ajax) == 1) {
		$index++;
		$code = 'end';
	} elseif (count($xuhuong_ajax) == 0) {
		$code = '0';
	}

	$arrayFeatureImage = array();

	foreach ($xuhuong_ajax as $key => $item) {
		$arrayFeatureImage[$key] = getFeaturedImageUrl($item->ID);
	}

	$arrayPermalink = array();

	foreach ($xuhuong_ajax as $key => $item) {
		$arrayPermalink[$key] = get_permalink($item->ID);
	}

	$arrayDescription = array();

	foreach ($xuhuong_ajax as $key => $item) {
		$arrayDescription[$key] = get_field('short_description', $item->ID);
	}

	$data = array('code' => $code, 'index' => $index, 'items' => $xuhuong_ajax, 'linkFeatureImg' => $arrayFeatureImage, 'permalink' => $arrayPermalink, 'descriptionCustomField' => $arrayDescription);

	echo json_encode($data);

	die();
}
add_action('wp_ajax_load_xuhuong', 'load_xuhuong_ajax_callback');





function load_itgirl_ajax_callback() {

	$index = ( isset($_POST['index']) ? $_POST['index'] : 0 );

    $args = array(
	    'numberposts' => 2,
	    'offset' => $index,
	    'orderby' => 'post_date',
	    'order' => 'DESC',
	    'post_type' => 'dep-nhu-it-girl',
	    'post_status' => 'publish',
	    'suppress_filters' => true );

	$itgirl_ajax = get_posts( $args );

	if (count($itgirl_ajax) == 2) {
		$index += 2;
		$code = '1';
	} elseif (count($itgirl_ajax) == 1) {
		$index++;
		$code = 'end';
	} elseif (count($itgirl_ajax) == 0) {
		$code = '0';
	}

	$arrayFeatureImage = array();

	foreach ($itgirl_ajax as $key => $item) {
		$arrayFeatureImage[$key] = getFeaturedImageUrl($item->ID);
	}

	$arrayPermalink = array();

	foreach ($itgirl_ajax as $key => $item) {
		$arrayPermalink[$key] = get_permalink($item->ID);
	}

	$arrayDescription = array();

	foreach ($itgirl_ajax as $key => $item) {
		$arrayJobTitle[$key] = get_field('job_title', $item->ID);
	}

	$data = array('code' => $code, 'index' => $index, 'items' => $itgirl_ajax, 'linkFeatureImg' => $arrayFeatureImage, 'permalink' => $arrayPermalink, 'jobTitle' => $arrayJobTitle);

	echo json_encode($data);

	die();
}
add_action('wp_ajax_load_itgirl', 'load_itgirl_ajax_callback');




function load_livestream_ajax_callback() {

	$index = ( isset($_POST['index']) ? $_POST['index'] : 0 );

    $args = array(
	    'numberposts' => 5,
	    'offset' => $index,
	    'orderby' => 'post_date',
	    'order' => 'DESC',
	    'post_type' => 'live-stream',
	    'post_status' => 'publish',
	    'suppress_filters' => true 
    );

	$livestream_ajax = get_posts( $args );

	if (count($livestream_ajax) == 2) {
		$index += 2;
		$code = '1';
	} elseif (count($livestream_ajax) == 1) {
		$index++;
		$code = 'end';
	} elseif (count($livestream_ajax) == 0) {
		$code = '0';
	}

	$arrayFeatureImage = array();

	foreach ($livestream_ajax as $key => $item) {
		$arrayFeatureImage[$key] = getFeaturedImageUrl($item->ID);
	}

	$arrayPermalink = array();

	foreach ($livestream_ajax as $key => $item) {
		$arrayPermalink[$key] = get_permalink($item->ID);
	}

	$arrayDescription = array();

	foreach ($livestream_ajax as $key => $item) {
		$arrayDescription[$key] = get_field('short_description', $item->ID);
	}

	$arrayLinkLiveStream = array();

	foreach ($livestream_ajax as $key => $item) {
		$arrayLinkLiveStream[$key] = get_field('link_youtube', $item->ID);
	}

	$data = array('code' => $code, 'index' => $index, 'items' => $livestream_ajax, 'linkFeatureImg' => $arrayFeatureImage, 'permalink' => $arrayPermalink, 'descriptionCustomField' => $arrayDescription, 'link_livestream' => $arrayLinkLiveStream);

	echo json_encode($data);

	die();
}
add_action('wp_ajax_load_livestream', 'load_livestream_ajax_callback');


function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
