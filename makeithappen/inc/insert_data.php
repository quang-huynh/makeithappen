<?php

function insert_user_into_db($username,$email,$first_name,$last_name,$phone_number,$gender,$birthday,$address) {
    $url = 'http://wifiads.vn/makeithappen/insert_users.php';
    $data = array(
                'username' => $username,
                'email' => $email,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone_number' => $phone_number,
                'gender'    => $gender,
                'birthday'  => $birthday,
                'address'   => $address
            );

    // use key 'http' even if you send the request to https://...
    // $options = array(
    //     'http' => array(
    //         'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
    //         'method'  => 'POST',
    //         'content' => http_build_query($data)
    //     )
    // );
    // $context  = stream_context_create($options);

    // $result = file_get_contents($url, false, $context);
    // if ($result === FALSE) { /* Handle error */ }


    // set post fields

    $ch = curl_init('http://wifiads.vn/makeithappen/insert_users.php');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    // execute!
    $response = curl_exec($ch);

    // close the connection, release resources used
    curl_close($ch);

    // do anything you want with your response
    // var_dump($response);


    return $response;

}

function create_account_callback(){
    //You may need some data validation here
    $username = ( isset($_POST['username']) ? $_POST['username'] : '' );
    $email = ( isset($_POST['email']) ? $_POST['email'] : '' );
    $password = ( isset($_POST['password']) ? $_POST['password'] : '' );
    $first_name = ( isset($_POST['first_name']) ? $_POST['first_name'] : '' );
    $last_name = ( isset($_POST['last_name']) ? $_POST['last_name'] : '' );
    $phone_number = ( isset($_POST['phone']) ? $_POST['phone'] : '' );
    $gender = ( isset($_POST['gender']) ? $_POST['gender'] : '' );
    $birthday = ( isset($_POST['birthday']) ? $_POST['birthday'] : '' );
    $address = ( isset($_POST['address']) ? $_POST['address'] : '' );

    if ( !username_exists( $username )  && !email_exists( $email ) ) {
        $user_id = wp_create_user( $username, $password, $email );
        if( !is_wp_error($user_id) ) {
            //user has been created
            $user = new WP_User( $user_id );
            $user->set_role( 'subscribe' );
            
            $response = 'Tạo tài khoản thành công!';
            $result_insert = insert_user_into_db($username,$email,$first_name,$last_name,$phone_number,$gender,$birthday,$address);
            $response .= $result_insert;

            echo json_encode(array('code' => '1', 'mess' => $response));

            wp_die();
        } else {
            echo json_encode(array('code' => '0', 'mess' => 'Tạo tài khoản chưa thành công. Vui lòng thử lại sau'));
            wp_die();
        }
    } else {
        echo json_encode(array('code' => '0', 'mess' => 'Tạo tài khoản chưa thành công. Username hoặc email đã có người sử dụng!'));
        wp_die();
    }

    wp_die();

}
// add_action('init','create_account');
add_action( 'wp_ajax_nopriv_create_account', 'create_account_callback' );
add_action('wp_ajax_create_account', 'create_account_callback');




function sb_admin_style_and_script() {
    wp_enqueue_script('sb-admin', '/js/q-custom.js', array('jquery'), false, true);
    wp_localize_script('sb-admin', 'sb_admin_ajax', array('url' => admin_url('admin-ajax.php')));
}
add_action('admin_enqueue_scripts', 'sb_admin_style_and_script');