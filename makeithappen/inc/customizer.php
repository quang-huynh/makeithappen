<?php


function themeslug_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'themeslug_logo_section' , array(
	    'title'       => __( 'Logo', 'themeslug' ),
	    'priority'    => 30,
	    'description' => 'Upload a logo to replace the default site name and description in the header',
	) );

	$wp_customize->add_setting( 'themeslug_logo' );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
	    'label'    => __( 'Logo', 'themeslug' ),
	    'section'  => 'themeslug_logo_section',
	    'settings' => 'themeslug_logo',
	) ) );


	// Add Custom Section
	$wp_customize->add_section('makeithappen_social_link_section', array(
	'title' => 'Social Link',
	'description' => 'Insert Social Link',
	'priority' => 120,
	));

	$wp_customize->add_setting( 'makeithappen_social_facebook_link', 
		array(
				'default' => 'www.facebook.com/index.php', //Default setting/value to save
	            'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
	            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
	            'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

	$wp_customize->add_control(
	'makeithappen_social_facebook_link', 
	array(
		'label'    => __( 'Facebook', 'ClickMedia - Make It Happen' ),
		'section'  => 'makeithappen_social_link_section',
		'settings' => 'makeithappen_social_facebook_link',
		'type'     => 'text',
	));

	$wp_customize->add_setting( 'makeithappen_social_youtube_link', 
		array(
				'default' => 'www.youtube.com/aaaa', //Default setting/value to save
	            'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
	            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
	            'transport' => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

	$wp_customize->add_control(
	'makeithappen_social_youtube_link', 
	array(
		'label'    => __( 'Youtube', 'ClickMedia - Make It Happen' ),
		'section'  => 'makeithappen_social_link_section',
		'settings' => 'makeithappen_social_youtube_link',
		'type'     => 'text',
	));


}
add_action( 'customize_register', 'themeslug_theme_customizer' );
