<?php get_header();?>
<?php 

    wpb_set_post_views(get_the_ID());

    global $post;

    $content = $post->post_content;



    //QUERY IT GIRL LIST
    $args = array(
        'numberposts' => 4,
        'offset' => 1,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'dep-nhu-it-girl',
        'post_status' => 'publish',
        'suppress_filters' => true );

    $itgirl = get_posts( $args );
?>
	





    <main class="detail-page">
        <section class="detail-article-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-xs-12 left-content">
                        <div class="row article-title">
                            <div class="col-xs-12 news-title">
                                <h3>ĐẸP NHƯ IT GIRL</h3>
                                <h1><?php echo $post->post_title ?><br/> 
                                    <!-- <strong><?php echo get_field('style_makeup'); ?></strong> -->
                                </h1>
                                <ul>
                                    <!-- <li><a href="#">Liquid Foundation</a><span>,</span></li>
                                    <li><a href="#">Compact Foundation</a><span>,</span></li>
                                    <li><a href="#">Skin</a><span>,</span></li>
                                    <li><a href="#">Natural Look</a><span>,</span></li> -->
                                </ul>

                                <div class="like-btn"><i class="fa fa-heart-o" aria-hidden="true"></i></div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-xs-12 article-item-description">
                                <div class="col-md-6 col-xs-12 left">
                                    <div class="box">
                                        <h3><?php echo get_field('head_product_sale_title'); ?></h3>
                                        <p><?php echo get_field('head_product_sale_description'); ?></p>
                                        <div class="like-btn"><i class="fa fa-heart-o" aria-hidden="true"></i></div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12 right">
                                    <h3><?php echo get_field('head_product_sale_title'); ?></h3>
                                    <div class="logo-brand">
                                        <img src="<?php echo get_field('head_product_sale_image'); ?>" alt="">
                                    </div>
                                    <a href="<?php echo get_field('head_product_sale_link'); ?>" target="_blank">SHOP NOW <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>

                        <?php if (!empty(get_field('set_items'))): ?>
                        <div class="row">
                            <div class="col-xs-12 article-item-list count-6">
                                <ul>
                                    <?php 
                                        foreach (get_field('set_items') as $key => $item) { ?>
                                            <li><a href="<?php echo $item['description']; ?>"><img src="<?php echo $item['url']; ?>" alt=""></a></li>
                                    <?php 
                                        } 
                                    ?>
                                </ul>
                                <div class="article-item-img-main">
                                    <img src="<?php echo get_field('profile_picture_center'); ?>" alt="">
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>


                        <div class="row">
                            <?php
                                if ( !empty( $content ) ) :
                                    echo $content;
                                endif;
                            ?>

                        </div>

                        <div class="row">
                            <div class="col-xs-12 article-actions">
                                <ul class="tag-list">
                                    <li><strong>TAGS:</strong></li>
                                    <?php 
                                        if (!empty(get_the_tags())) {
                                            foreach (get_the_tags() as $key => $tag) { 
                                    ?>
                                            <li><a href="#"><?php echo $tag->name; ?></a></li>
                                    <?php } } ?>
                                </ul>

                                <ul class="social-link">
                                    <li><a href="<?php echo get_field('link_facebook'); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="<?php echo get_field('link_youtube'); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <?php //include_once('comment.php'); ?>

                        <div class="row my-style my-style-custom">
                            <div class="col-xs-12">
                                <h1>BẠN CŨNG SẼ THÍCH</h1>
                            </div>


                            <?php 
                                if (!empty($itgirl)):
                                    foreach ($itgirl as $key => $item) { ?>
                                    <div class="col-sm-3 col-xs-6 my-style-block">
                                        <div class="my-style-img" style="background-image: url(<?php echo getFeaturedImageUrl($item->ID); ?>)">
                                            <a href="<?php echo get_permalink($item->ID); ?>"><img src="<?php echo getFeaturedImageUrl($item->ID); ?>" alt=""></a>
                                        </div>
                                        <div class="my-style-info">
                                            <h3><a href="<?php echo get_permalink($item->ID); ?>"><?php echo get_field('title', $item->ID); ?></a></h3>
                                        </div>
                                    </div>
                            <?php 
                                    } 
                                endif;
                            ?>
                        </div>
                    </div>


                    <?php include_once( 'sidebar.php' ); ?>

                </div>

            </div>
        </section>
    </main>

<?php get_footer();?>