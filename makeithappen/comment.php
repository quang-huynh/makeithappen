<div class="row">
    <div class="col-xs-12 posted-by">
        <div class="col-md-3 col-xs-12 posted-avatar">
            <img src="../images/avatar.jpg" alt="">
        </div>

        <div class="col-md-9 col-xs-12 posted-content">
            <h3>POSTED BY ADMIN</h3>
            <p>Lorem ipsum dolor sit amet, ludus audiam assueverit nec ex. Cu assum phaedrum cum, placerat comprehensam his an. Eum ad viris affert. Mazim detracto appetere ut his, sed id atqui expetendis, illud phaedrum tractatos sit at. Vel an atqui eirmod salutandi, prima dicant ei vel.</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 comment-list">
        <h3 class="title-cmt">COMMENT (1)</h3>
        <ul class="media-list">
            <li class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="../images/avatar.jpg" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Media heading</h4>
                    <p>Lorem ipsum dolor sit amet, ludus audiam assueverit nec ex. Cu assum phaedrum cum, placerat comprehensam his an.</p>

                    <div class="media-actions">
                        <div class="media-actions-btn media-reply">- REPLY -</div>
                        <div class="media-actions-btn media-share"><a href="#">- SHARE -</a></div>
                    </div>

                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="../images/avatar.jpg" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Media heading</h4>
                            <p>Lorem ipsum dolor sit amet, ludus audiam assueverit nec ex. Cu assum phaedrum cum, placerat comprehensam his an.</p>

                            <div class="media-actions">
                                <div class="media-actions-btn media-reply">- REPLY -</div>
                                <div class="media-actions-btn media-share"><a href="#">- SHARE -</a></div>
                            </div>
                        </div>
                    </div>

                    <div class="media reply-box">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="../images/avatar.jpg" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <p>aaa</p>
                            <textarea name="" id="" placeholder="Leave a message"></textarea>

                            <div class="media-actions">
                                <div class="media-actions-btn media-reply">- REPLY -</div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="../images/avatar.jpg" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Media heading</h4>
                    <p>Lorem ipsum dolor sit amet, ludus audiam assueverit nec ex. Cu assum phaedrum cum, placerat comprehensam his an.</p>

                    <div class="media-actions">
                        <div class="media-actions-btn media-reply">- REPLY -</div>
                        <div class="media-actions-btn media-share"><a href="#">- SHARE -</a></div>
                    </div>

                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="../images/avatar.jpg" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Media heading</h4>
                            <p>Lorem ipsum dolor sit amet, ludus audiam assueverit nec ex. Cu assum phaedrum cum, placerat comprehensam his an.</p>

                            <div class="media-actions">
                                <div class="media-actions-btn media-reply">- REPLY -</div>
                                <div class="media-actions-btn media-share"><a href="#">- SHARE -</a></div>
                            </div>
                        </div>
                    </div>

                    <div class="media reply-box">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="../images/avatar.jpg" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <p>aaa</p>
                            <textarea name="" id="" placeholder="Leave a message"></textarea>

                            <div class="media-actions">
                                <div class="media-actions-btn media-reply">- REPLY -</div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="col-xs-12 comment-box">
        <h3 class="title-cmt">REPLY COMMENT</h3>
        <form action="">
            <div class="form-inner">
                <input type="text" name="name-cmt" placeholder="Name">
                <input type="text" name="email-cmt" placeholder="Email">
            </div>

            <div class="form-inner">
                <textarea name="content-cmt" placeholder="Message"></textarea>
            </div>

            <div class="media-actions">
                <div class="media-actions-btn media-reply">- POST REPLY -</div>
            </div>
        </form>
    </div>
</div>