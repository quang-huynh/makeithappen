<?php
/**
 * Header template (header.php)
 * @package WordPress
 * @subpackage ClickMedia-Make-It-Happen-template
 */

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>

        <?php
            $current = 'subpage-header';
            if (function_exists('is_tag') && is_tag()) {
                single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; 
            }
            elseif (is_archive()) {
                wp_title(''); echo ' Archive - '; 
            }
            elseif (is_search()) {
                echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; 
            }
            elseif (!(is_404()) && (is_single()) || (is_page())) {
                wp_title(''); echo ' - '; 
            }
            elseif (is_404()) {
                echo 'Not Found - '; 
            }
            if (is_home()) {
                // bloginfo('name'); echo ' - '; bloginfo('description'); $current = 'home-header'; }
                echo 'Home - '; bloginfo('description'); $current = 'home-header'; 
            } else {
                bloginfo('name'); 
            }
            if ($paged>1) {
                echo ' - page '. $paged; 
            }

        ?>

    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <link rel="icon" href="<?php bloginfo('template_url')?>/images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css"/>

    <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/bootstrap.css">

    <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/style.css">

    <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/font-awesome.min.css">

    <link href="<?php bloginfo('stylesheet_url');?>" type="text/css" rel="stylesheet" />

    <?php wp_head();?>

    
    <script type="text/javascript">
        var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
    </script>

</head>

<body>


<header>
    <div class="header-top-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 header-top">
                    <!--change language-->
                    <div class="select-custom">
                        <div class="select-custom-choosen"><span>En</span> <i class="fa fa-angle-down" aria-hidden="true"></i></div>
                        <ul>
                            <li>En</li>
                            <li>Vie</li>
                        </ul>
                    </div>

                    <ul class="social-link">
                        <li><a href="<?php echo get_theme_mod( 'makeithappen_social_facebook_link' ); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="<?php echo get_theme_mod( 'makeithappen_social_youtube_link' ); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="header-main-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 header-main">
                    <div class="logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>" alt=""></a>
                    </div>

                    <div class="menu-hambergur"><span></span></div>

                    <?php echo wp_nav_menu(array('menu' => 'Main-Menu', 'menu_class'  => 'main-menu navigation', 'theme_location' => 'primary' )); ?>

                    <div class="search">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                    
                    <div class="search-box">
                        <input type="text" placeholder="Bạn đang tìm kiếm gì...">
                        <div class="close-search-box"><i class="fa fa-times" aria-hidden="true"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>